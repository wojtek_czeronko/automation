Introduction
============
The purpose of this project is to on board 3rd parties faster. We want to reduce the feedback loop and increase the speed of developing a cartridge.

There are two types of cartridge: eCommerce Adapters and PSPs.

In order to do this, we have provided 3rd parties access to call their cartridge via a test sandbox shop. We have provided a list of samples to develop against. These express scenarios for our following API endpoints:

*  _Get Products_
Retrieves a product after scanning a tag

*  _Calculate Costs_
Determines Subtotal, Shipping, Tax and Total costs for the purchase

*  _Process Orders_
Places an order with payment details included

*  _Confirm Payment_
Sends a payment

Installation
============
1. Install Google Chrome https://www.google.com/intl/en_uk/chrome/browser/
2. Install Postman - REST Client in Google Chrome https://chrome.google.com/webstore/detail/postman-rest-client/fdmmgilgnpjigdojojpjoooidkmcomcm?hl=en
3. Go to Postman - REST Client

Instructions
============
Below are instructions to call the API methods in POWA Core:

Notes:

*  `<shopID>` refers to your given shopID (that we've provided you)
*  `<productID>` is the unique ID / SKU to reference each product
*  `<baseURL>` is constant for all endpoints
*  `Your URL` is the endpoint that you must expose

Get Products
------------
| Type | Example |
| --- | --- |
| Headers | Accept : application/json, Content-Type: application/json |
| HTTP Request | GET |
| POWA Core URL | http://176.34.138.84/shops/`<shopID>`/products/`<productID>` |
| Your URL | `<baseURL>`/products/`<productID>` |

Calculate Costs
---------------
| Type | Example |
| --- | --- |
| Headers | Accept : application/json, Content-Type: application/json |
| HTTP Request | POST |
| HTTP Request Body | Please refer to requests in `json/samples/request/` |
| POWA Core URL | http://176.34.138.84/shops/`<shopID>`/orders/costs |
| Your URL | `<baseURL>`/orders/costs |

Process Orders
--------------
| Type | Example |
| --- | --- |
| Headers | Accept : application/json, Content-Type: application/json |
| HTTP Request | POST |
| HTTP Request Body | Please refer to requests in `json/samples/request/` |
| POWA Core URL | http://176.34.138.84/shops/`<shopID>`/orders |
| Your URL | `<baseURL>`/orders |

Confirm Payment
---------------
(TODO)

Samples
=======
The scenerios required to be covered are:

Get Products
------------
|   | Response |
|---|----------|
| 1. | [One variant, no options](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/getProducts-1.json) |
| 2. | [One variant, multiple options](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/getProducts-2.json?at=master) |
| 3. | [Multiple variants, multiple options](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/getProducts-3.json?at=master) |
| 4. | [Multiple prices](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/getProducts-4.json?at=master) |
| 5. | [Multiple product images](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/getProducts-5.json?at=master) |
| 6. | [Stock is null](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/getProducts-6.json?at=master) Must still display 0 |
| 7. | [Stock is 0](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/getProducts-7.json?at=master) Same as above (Number 6 – stock is null) |
| 8. | [Stock is not null or 0](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/getProducts-8.json?at=master) |
| 9. | [Multiple variants, some where stock is 0](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/getProducts-9.json?at=master) |
| 10. | [SKU not found](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/getProducts-10.json?at=master) |

Calculate Costs
---------------
|   | Request | Response |
|---|---------|----------|
|1. | [One variant, no options](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/calculateCosts-1.json?at=master) | [One variant, no options](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/calculateCosts-1.json?at=master) |
|2. | [One variant, multiple options](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/calculateCosts-2.json?at=master) | [One variant, multiple options](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/calculateCosts-2.json?at=master) |
|3. | [Multiple variants, multiple options](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/calculateCosts-3.json?at=master) | [Multiple variants, multiple options](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/calculateCosts-3.json?at=master) |
|4. | [Multiple prices](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/calculateCosts-4.json?at=master) | [Multiple prices](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/calculateCosts-4.json?at=master) |
|5. | [Stock is 0](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/calculateCosts-5.json?at=master) | [Stock is 0](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/calculateCosts-5.json?at=master) |
|6. | [One product, quantity > stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/calculateCosts-6.json?at=master) | [One product, quantity > stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/calculateCosts-6.json?at=master) |
|7. | [Multiple products, all quantity < stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/calculateCosts-7.json?at=master) | [Multiple products, all quantity < stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/calculateCosts-7.json?at=master) |
|8. | [Multiple products, some quantity < stock, some quantity = stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/calculateCosts-8.json?at=master) | [Multiple products, some quantity < stock, some quantity = stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/calculateCosts-8.json?at=master) |
|9. | [Multiple products, some quantity < stock, some quantity = stock, some quantity > stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/calculateCosts-9.json?at=master) | [Multiple products, some quantity < stock, some quantity = stock, some quantity > stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/calculateCosts-9.json?at=master) |
|10.| [Multiple products, all quantity > stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/calculateCosts-10.json?at=master) | [Multiple products, all quantity > stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/calculateCosts-10.json?at=master) |

Process Orders
--------------
|   | Request | Response |
|---|---------|----------|
|1. | [One variant, no options](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/processOrders-1.json?at=master) | [One variant, no options](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/processOrders-1.json?at=master) |
|2. | [One variant, multiple options](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/processOrders-2.json?at=master) | [One variant, multiple options](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/processOrders-2.json?at=master) |
|3. | [Multiple variants, multiple options](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/processOrders-3.json?at=master) | [Multiple variants, multiple options](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/processOrders-3.json?at=master) |
|4. | [Multiple prices](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/processOrders-4.json?at=master) | [Multiple prices](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/processOrders-4.json?at=master) |
|5. | [Stock is 0](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/processOrders-5.json?at=master) | [Stock is 0](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/processOrders-5.json?at=master) |
|6. | [One product, quantity > stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/processOrders-6.json?at=master) | [One product, quantity > stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/processOrders-6.json?at=master) |
|7. | [Multiple products, all quantity < stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/processOrders-7.json?at=master) | [Multiple products, all quantity < stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/processOrders-7.json?at=master) |
|8. | [Multiple products, some quantity < stock, some quantity = stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/processOrders-8.json?at=master) | [Multiple products, some quantity < stock, some quantity = stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/processOrders-8.json?at=master) |
|9. | [Multiple products, some quantity < stock, some quantity = stock, some quantity > stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/processOrders-9.json?at=master) | [Multiple products, some quantity < stock, some quantity = stock, some quantity > stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/processOrders-9.json?at=master) |
|10.| [Multiple products, all quantity > stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/request/processOrders-10.json?at=master) | [Multiple products, all quantity > stock](https://bitbucket.org/powaaim/automation/src/master/json/samples/response/processOrders-10.json?at=master) |

Process Payment
---------------
Following are the transaction types POWA Core supports

1. DEFERRED - Sets the funds ready to be taken once a CAPTURE transaction occurs. It places a "shadow" on the customer's
   card to ensure that the funds aren't spent elsewhere. A DEFERRED transaction can only be CAPTUREd once.
   If the DEFERRED transaction isn't CAPTUREd in the next 6 days, the 'shadow' disappears and there is no guarantee that
   the funds will be available.
2. CAPTURE - Marks the transaction as ready to be settled with merchant’s acquiring bank. Until CAPTUREd no funds are
   taken for such transaction. It needs a previous DEFERRED transaction.
3. PAYMENT - Also called DEFERRED + CAPTURE. This transaction is a two-step process. It performs a DEFERRED,
   immediately followed by a CAPTURE. It is the most popular transaction type despite its associated risks.
4. REFUND - Also known as CREDIT. The issuing bank for the credit card takes money out of the merchant's bank account
   and returns it to the customer. The Refund can be for the entire transaction or any part of it. Multiple refunds can
   be performed against the same payment as far as they don’t exceed the amount of the original transaction.
5. VOID - Similar to REFUND but only valid for transactions that haven’t been settled yet. No partial VOIDs are
   accepted, it only applies to the full amount of the original transaction.  Another difference of VOID with REFUND
   is that the former does not imply bank fees for the merchant.
6. ABORT - Cancels a DEFERRED transaction to ensure it is never settle down with the merchant's bank.

JSON Request/Responses
----------------------
1. DEFERRED - ProcessPayment-1.json
2. CAPTURE - ProcessPayment-2.json (get the custom fields from DEFERRED transaction and use it in CAPTURE request)
3. PAYMENT - ProcessPayment-3.json
4. REFUND - ProcessPayment-4.json (get the custom fields from CAPTURE or PAYMENT transaction and use it in REFUND
   request)
5. VOID - ProcessPayment-5.json (get the custom fields from CAPTURE, PAYMENT or REFUND transaction and use it in VOID
   request)
6. ABORT - ProcessPayment-6.json (get the custom fields from DEFERRED transaction and use it in ABORT request)

Test Scenarios to cover:
See the file PSP_Adapter_TestCases.xlsx


Handling error conditions
-------------------------
In regards to handling error conditions, we expect to ALWAYS receive JSON responses in the following format:

    {
     "code": "BAD_REQUEST",
     "message": "The request entity had validation errors",
     "validationErrors": [ "user.emailAddress may not be empty" ]
    }
Handled error conditions

| code | HTTP Response Code | 
| --- | --- |
| BAD_REQUEST |	400 |
| SHOP_NOT_FOUND | 404 |
| SKU_NOT_FOUND	| 404 |
| NOT_IN_STOCK	| 400 |
| INVALID_PAYMENT |	400  |
| UNEXPECTED_ERROR | 500 |

Notes:

* All error responses must be a non 200 HTTP Response.
* The code field must be one of the following codes in the table above.
* The message and validation error fields are not standardised. They are just used for debugging purposes.

## Hmac key implementation ##

All the requests sent by PowaTag include a "hmac" header which is generated from a secret key (hmacKey)  shared by the Powa team and the merchant, along with the data sent in the request.

To verify that the request came from PowaTag, compute the HMAC digest according to the algorithm specified below and compare it to the value in the "hmac" header. If they match, you can be sure that the json message was sent from PowaTag and the data has not been compromised. 

1. Concatenate the hmacKey + jsonEntityString  together (In getProduct request this will be null as you do not get an entity from us)
2. With the algorithm Sha-256 hash the (hmacKey + jsonEntityString) to give you an array of bytes
3. Convert this to Base64

**Example of what we sent in the header**

hmac: oDsiHGxurnEiylFpXUVtUiLlJIiRNjlJRLL5djtINhU=

**Hmac key implementation in different programming languages:**

### JAVA ###

```
#!java

private String buildHmac(ObjectMapper objectMapper, String hmacKey, Object objEntity) throws Exception {
 
        String ret;
        try {
            String entity = objectMapper.writeValueAsString(objEntity);
            MessageDigest messageDigest = MessageDigest.getInstance("sha-256");
            byte[] digest = messageDigest.digest((hmacKey + entity).getBytes("UTF-8"));
            ret = DatatypeConverter.printBase64Binary(digest);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Unable to build hmac key");
        }
        return ret;
    }
```


### PHP ###


```
#!php

<?php
 
    $hmac_key = "12345abc";
    $body = '';
    $calculatedHmac = base64_encode(hash ('sha256', $hmac_key . $body, true));
 
?>
```


Appendices
==========
Appendix 1
----------
(See 1.png)

Appendix 2
----------
{
   "order":{
      "customer":{
         "firstName":"Joe",
         "lastName":"Bloggs",
         "emailAddress":"joebloggs@powa.com",
         "phone":"07890123456",
         "shippingAddress":{
            "friendlyName":"Default Address",
            "firstName":"Joe",
            "lastName":"Bloggs",
            "line1":"1 Bryant Park",
            "line2":"Suite 4444",
            "city":"New York",
            "county":"",
            "state":"NY",
            "postCode":"10036",
            "country":{
               "name":"United States",
               "alpha2Code":"US"
            }
         }
      },
      "orderLineItems":[
         {
            "product":{
               "name":"T-Shirt",
               "code":"TSHIRT",
               "productVariants":[
                  {
                     "code":"TSHIRT1",
                     "numberInStock":8,
                     "originalPrice":{
                        "amount":17.99,
                        "currency":"GBP"
                     },
                     "finalPrice":{
                        "amount":15.99,
                        "currency":"GBP"
                     },
                     "options":{
                        "Color":"Blue",
                        "Size":"Small"
                     }
                  }
               ]
            },
            "quantity":1
         }
      ]
   }
}

Appendix 3
----------
(See 3.png)

Appendix 4
----------
{
   "orders":[
      {
         "customer":{
            "firstName":"Joe",
            "lastName":"Bloggs",
            "emailAddress":"joebloggs@powa.com",
            "phone":"07890123456",
            "shippingAddress":{
               "friendlyName":"Default Address",
               "firstName":"Joe",
               "lastName":"Bloggs",
               "line1":"1 Bryant Park",
               "line2":"Suite 4444",
               "city":"New York",
               "county":"",
               "state":"NY",
               "postCode":"10036",
               "country":{
                  "name":"United States",
                  "alpha2Code":"US"
               }
            }
         },
         "orderCostSummary":{
            "subTotal":{
               "amount": 91.98,
               "currency":"USD"
            },
            "shippingCost":{
               "amount":3.00,
               "currency":"USD"
            },
            "tax":{
               "amount":2.50,
               "currency":"USD"
            },
            "total":{
               "amount":97.48,
               "currency":"USD"
            }
         },
         "orderLineItems":[
            {
               "product":{
                  "name":"T-Shirt",
                  "code":"TSHIRT",
                  "productVariants":[
                     {
                        "code":"TSHIRT1",
                        "numberInStock":8,
                        "originalPrice":{
                           "amount":17.99,
                           "currency":"GBP"
                        },
                        "finalPrice":{
                           "amount":15.99,
                           "currency":"GBP"
                        },
                        "options":{
                           "Color":"Blue",
                           "Size":"Small"
                        }
                     }
                  ]
               },
               "quantity":1
            }
         ],
         "device":{
            "deviceID":"990000862471854",
            "ipAddress":"127.0.0.1",
            "location":{
               "latitude":234.0,
               "longitude":3456.0
            }
         },
         "paymentCard":{
            "friendlyName":"My Visa",
            "number":"4444333322221111",
            "cvv": "123",
            "type":"AMEX",
            "cardHolderName":"Joe Bloggs",
            "validFrom": "2012/10",
            "expirationDate":"2020/08",
            "issueNumber": "01",
            "billingAddress":{
               "friendlyName":"Default Address",
               "firstName":"Joe",
               "lastName":"Bloggs",
               "line1":"1 Bryant Park",
               "line2":"Suite 4444",
               "city":"New York",
               "county":"",
               "state":"NY",
               "postCode":"10036",
               "country":{
                  "name":"United States",
                  "alpha2Code":"US"
               }
            }
         }
      }
   ]
}

Appendix 5
----------
(See 5.png)

Data Fields
===========

Get Products Response 
---------------------
| Name | Description | Example | Nullable | Type | | Notes |
| --- | --- | --- | --- | --- | --- | --- |
| Name | Name of the product | T-Shirt | No | String | | |
| Type  | Has to be "PRODUCT, DONATION, LEADGEN, WARRANTY, PAYWITHTIP, PAYNOTIP, TICKETING, PRODUCTWITHSCHEDULEDDELIVERY"| PRODUCT | No | ProductType | |
| Available Currencies | Array of available currencies in ISO 4217 format | GBP, EUR, USD | No | String list | |
| Code  | SKU code of the product |TSHIRT | No | String | |
| Description | Long description of the product | This is a cotton T-Shirt | Yes | String | |
| Currency | Currency in ISO 4217 format | GBP | No | String | |
| Language | "Microformat such as en-GB, en-US" | en-GB | No | String | |
| Product Images| Array to hold product images | | Yes |ProductImage list | | "Can be null but if included, must also follow below conditions" |
| Product Images - Name | Product Image Name | Front | No | String | | Must be included and not null and empty when productImages - url is specified |
| Product Images - URL | Product Image URL | http://url.to/image_1.jpg | No | String | | Must be included and not null and empty when productImages - name is specified |
| Product Options | Array to hold product options |  | Yes | ProductOption map |  |
| Product Options - Key | Top level key | "Color, Size" | No | String |  | Label displayed in the mobile app for the options dropdown list |
| Product Options - Key - ID | ID of product options key | 123 | No | String |  |
| Product Options - Key - Values | Array of possible possible options | "Red, Blue, Green" | No | String list | | Each of the array items corresponds with an option displayed in the options dropdown list| 
| Product Variants | Array to hold product variants | | No | ProductVariant list | |
| Product Variants - Code | Order Line Item Product SKU Code | TSHIRT1-BLUE | No | String | | Identifier for the specific product variant |
| Product Variants - Number In Stock | Order Line Item Number in Stock | 10 | No | int | |
| Product Variants - Original Price | Object to hold original price | | Yes | Money | |
| Product Variants - Original Price - Amount | Order Line Item Product Original Price Amount in the associated ISO 4217 format | 12.34 | No | BigDecimal | |
| Product Variants - Original Price - Currency | Order Line Item Product Original Price Currency in ISO 4217 format| GBP | No | Currency | |
| Product Variants - Final Price | Object to hold final price | | No | Money | |
| Product Variants - Final Price - Amount | Order Line Item Product Final Price Amount in the associated ISO 4217 format |11.22 | No | BigDecimal | | If the product is on sale, this is the sale price. If not, final price and original price are the same |
| Product Variants - Final Price - Currency | Order Line Item Product Final Price Currency in ISO 4217 format | GBP | No | Currency | |
| Product Variants - Options | Map to hold options | | No | String map | | Specific Options for the product variant
| Product Variants - Options - key | Options key | Size | No | String | | This field has to match one of the values in the list of Product.Options.key |
| Product Variants - Options - value | Options value | Small | No | String | |
| Product Attributes | Map used to display non-choosable attributes that apply to all variants | | Yes | String map | |
| Product Attributes - key | Attribute key | Material | No | String | |
| Product Attributes - value | Attribute value | 100% Cotton | No | String | |
| Custom Fields | Map of free text field | | Yes | String map | |
| Custom Fields - key | Custom Field key | name | No | String | |
| Custom Fields - value | Custom Field value | Gift Message | No | String | |

Calculate Costs Request
-----------------------
| Name | Description | Example | Nullable | Type | | Notes |
| --- | --- | --- | --- | --- | --- | --- |
| Customer | Describes a Customer |  | No |  |  | 
| Customer - First Name | Customer's First Name | Joe | No | String |  |  |
| Customer - Last Name | Customer's Last Name | Bloggs | No | String |  |  |
| Customer - Email Address | Customer's Email Address | joebloggs@powa.com | No | String |  | Should pass email validation checks |
| Customer - Shipping Address | Customer's Shipping Address |  | No | Address |  |  |
| Customer - Shipping Address - Friendly Name | Address Friendly Name | Default Address | Yes | String |  |  |
| Customer - Shipping Address - First Name | Address Person First Name | Joe | No | String |  |  |
| Customer - Shipping Address - Last Name | Address Person Last Name | Bloggs | No | String |  |  |
| Customer - Shipping Address - Line 1 | Address Line 1 | 1 Bryant Park | No | String |  |  |
| Customer - Shipping Address - Line 2 | Address Line 2 | Suite 4444 | Yes | String |  |  |
| Customer - Shipping Address - Line 3 | Address Line 3 | | Yes | String |  |  |
| Customer - Shipping Address - Line 4 | Address Line 4 | | Yes | String |  |  |
| Customer - Shipping Address - City | Address City | New York | No | String |  |  |
| Customer - Shipping Address - County | Address County |  | Yes | String |  |  |
| Customer - Shipping Address - State | Address State | NY | Yes/No | String |  |Mandatory field for US and Canada. Optional for UK. Normally one of both County/State mustn't be null. |
| Customer - Shipping Address - Post Code | Address Post Code | 10036 | No | String |  |  |
| Customer - Shipping Address - Country | Address Country |  | No | Country |  |  |
| Customer - Shipping Address - Country - Name | Address Country Name | United States | Yes | String |  |  |
| Customer - Shipping Address - Country - Alpha 2 Code | Address Country Alpha 2 Code | US | No | String |  |  |
| Order Line Items | List of products requested |  | No |  |  |  |
| Order Line Items - Product | Product information |  | No |  |  |  |
| Order Line Items - Product - Name | Name of the product | T-Shirt | No | String |  |  |
| Order Line Items - Product - Code | SKU code of the product | TSHIRT1 | No | String |  |  |
| Order Line Items - Product - Product Variants | Array to hold product variants |  | No |  |  |  |
| Order Line Items - Product - Product Variants - Code | Order Line Item Product SKU Code | TSHIRT1-BLUE | No | String |  | If a product has no variants, still there must be one Product Variant whose code is equals to the Product Code |
| Order Line Items - Product - Product Variants - Original Price | Object to hold original price |  | Yes |  |  | 
| Order Line Items - Product - Product Variants - Original Price - Amount | Order Line Item Product Original Price Amount in the associated ISO 4217 format | 12.34 | Yes/No | BigDecimal |  | |
| Order Line Items - Product - Product Variants - Original Price - Currency | Order Line Item Product Original Price Currency in ISO 4217 format | GBP | Yes | Currency |  |  |
| Order Line Items - Product - Product Variants - Final Price | Object to hold final price |  | No |  |  |  |
| Order Line Items - Product - Product Variants - Final Price - Amount | Order Line Item Product Final Price Amount in the associated ISO 4217 format | 11.22 | No | BigDecimal |  |  |
| Order Line Items - Product - Product Variants - Final Price - Currency | Order Line Item Product Final Price Currency in ISO 4217 format | GBP | No | Currency |  |  |
| Order Line Items - Product - Product Variants - Options | Map to hold options |  | Yes |  |  |  |
| Order Line Items - Product - Product Variants - Options - key | Options key | Size | Yes | String |  |  |
| Order Line Items - Product - Product Variants - Options - value | Options value | Small | Yes | String |  |  |
| Order Line Items - Quantity | Quantity of product requested | 2 | No | Integer |  |  |

Calculate Costs Response
------------------------
| Name | Description | Example | Nullable | Type | | Notes |
| --- | --- | --- | --- | --- | --- | --- |
Order Cost Summary | Object to hold cost breakdown |  | No |  |  |  |
Order Cost Summary - Shipping Cost | Cost to ship items |  | No |  |  |  |
Order Cost Summary - Shipping Cost - Amount | Amount in the associated ISO 4217 format | 91.98 | No | BigDecimal |  |  |
Order Cost Summary - Shipping Cost - Currency | Currency in ISO 4217 format | USD | No | Currency |  |  |
Order Cost Summary - Sub Total | Price of items before shipping and tax |  | No |  |  |  |
Order Cost Summary - Sub Total - Amount | Format as described in shipping cost amount | 3.00 | No | BigDecimal |  |  |
Order Cost Summary - Sub Total - Currency | Format as described in shipping cost currency | USD | No | Currency |  |  |
Order Cost Summary - Tax | Tax payable on items |  | No |  |  |  |
Order Cost Summary - Tax - Amount | Format as described in shipping cost amount | 2.50 | No | BigDecimal |  |  |
Order Cost Summary - Tax - Currency | Format as described in shipping cost currency | USD | No | Currency |  |  |
Order Cost Summary - Total | Total price of items |  | No |  |  |  |
Order Cost Summary - Total - Amount | Format as described in shipping cost amount | 97.48 | No | BigDecimal |  |  |
Order Cost Summary - Total - Currency | Format as described in shipping cost currency | USD | No | Currency |  |  |

Process Orders Request
----------------------
| Name | Description | Example | Nullable | Type | | Notes |
| --- | --- | --- | --- | --- | --- | --- |
| Orders | Top level array of orders |  | No |  |  |  |
| Order ID |  |  | Yes/No |  |  | Required for one and two step, optional for three steps strategy |
| Customer | Describes a Customer |  | No |  |  |  |
| Customer - First Name | Customer's First Name | Joe | No | String |  |  |
| Customer - Last Name | Customer's Last Name | Bloggs | No | String |  |  |
| Customer - Email Address | Customer's Email Address | joebloggs@powa.com | No | String |  | Should pass email validation checks |
| Customer - Shipping Address | Customer's Shipping Address |  | No |  |  |  |
| Customer - Shipping Address - Friendly Name | Address Friendly Name | Default Address | Yes | String |  |  |
| Customer - Shipping Address - First Name | Address Person First Name | Joe | No | String |  |  |
| Customer - Shipping Address - Last Name | Address Person Last Name | Bloggs | No | String |  |  |
| Customer - Shipping Address - Line 1 | Address Line 1 | 1 Bryant Park | No | String |  |  |
| Customer - Shipping Address - Line 2 | Address Line 2 | Suite 4444 | Yes | String |  |  |
| Customer - Shipping Address - Line 3 | Address Line 3 | | Yes | String |  |  |
| Customer - Shipping Address - Line 4 | Address Line 4 | | Yes | String |  |  |
| Customer - Shipping Address - City | Address City | New York | No | String |  |  |
| Customer - Shipping Address - County | Address County |  | Yes | String |  |  |
| Customer - Shipping Address - State | Address State | NY | Yes/No | String |  | Mandatory for US and Canada. Optional for UK |
| Customer - Shipping Address - Post Code | Address Post Code | 10036 | No | String |  |  |
| Customer - Shipping Address - Country | Address Country |  | No | String |  |  |
| Customer - Shipping Address - Country - Name | Address Country Name | United States | Yes | String |  |  |
| Customer - Shipping Address - Country - Alpha 2 Code | Address Country Alpha 2 Code | US | No | String |  |  |
| Order Cost Summary | Object to hold cost breakdown |  | No |  |  |  |
| Order Cost Summary - Sub Total | Cost to ship items |  | No |  |  |  |
| Order Cost Summary - Sub Total - Amount | Amount in the associated ISO 4217 format | 91.98 | No | BigDecimal |  |  |
| Order Cost Summary - Sub Total - Currency | Currency in ISO 4217 format | USD | No | Currency |  |  |
| Order Cost Summary - Shipping Cost | Price of items before shipping and tax |  | No |  |  |  |
| Order Cost Summary - Shipping Cost - Amount | Format as described in shipping cost amount | 3.00 | No | BigDecimal |  |  |
| Order Cost Summary - Shipping Cost - Currency | Format as described in shipping cost currency | USD | No | Currency |  |  |
| Order Cost Summary - Tax | Tax payable on items |  | No |  |  |  |
| Order Cost Summary - Tax - Amount | Format as described in shipping cost amount | 2.50 | No | BigDecimal |  |  |
| Order Cost Summary - Tax - Currency | Format as described in shipping cost currency | USD | No | Currency |  |  |
| Order Cost Summary - Total | Total price of items |  | No |  |  |  |
| Order Cost Summary - Total - Amount | Format as described in shipping cost amount | 97.48 | No | BigDecimal |  |  |
| Order Cost Summary - Total - Currency | Format as described in shipping cost currency | USD | No | Currency |  |  |
| Order Line Items | List of products requested |  | No |  |  |  |
| Order Line Items - Product | Product information |  | No |  |  |  |
| Order Line Items - Product - Name | Name of the product | T-Shirt | No | String |  |  |
| Order Line Items - Product - Code | SKU code of the product | TSHIRT1 | No | String |  |  |
| Order Line Items - Product - Product Variants | Array to hold product variants |  | No |  |  |  |
| Order Line Items - Product - Product Variants - Code | Order Line Item Product SKU Code | TSHIRT1-BLUE | No | String |  |  |
| Order Line Items - Product - Product Variants - Original Price | Object to hold original price |  | Yes |  |  |  |
| Order Line Items - Product - Product Variants - Original Price - Amount | Order Line Item Product Original Price Amount in the associated ISO 4217 format | 12.34 | Yes | BigDecimal |  |  |
| Order Line Items - Product - Product Variants - Original Price - Currency | Order Line Item Product Original Price Currency in ISO 4217 format | GBP | Yes | Currency |  |  |
| Order Line Items - Product - Product Variants - Final Price | Object to hold final price |  | No |  |  |  |
| Order Line Items - Product - Product Variants - Final Price - Amount | Order Line Item Product Final Price Amount in the associated ISO 4217 format | 11.22 | No | BigDecimal |  |  |
| Order Line Items - Product - Product Variants - Final Price - Currency | Order Line Item Product Final Price Currency in ISO 4217 format | GBP | No | Currency |  |  |
| Order Line Items - Product - Product Variants - Options | Map to hold options |  | Yes |  |  |  |
| Order Line Items - Product - Product Variants - Options - key | Options key | Size | Yes | String |  |  |
| Order Line Items - Product - Product Variants - Options - value | Options value | Small | Yes | String |  |  |
| Order Line Items - Quantity | Quantity of product requested |  | No | Integer |  |  |
| Device | Device information |  | Yes |  |  |  |
| Device - Device ID | ID in e.g. IMEI, Serial Number, etc. format | 990000862471854 | Yes | String |  |  |
| Device - IP Address | IP Address of the client for fraud detection |  | Yes | String |  |  |
| Device - Location | Geolocation coordinates |  | Yes | Location |  |  |
| Device - Location - Latitude | Geolocation coordinates - latitude |  | Yes | Double |  |  |
| Device - Location - Longitude | Geolocation coordinates - longitude |  | Yes | Double |  |  |
| Payment Card | Payment card information |  | No |  |  |  |
| Payment Card - Friendly Name | Name of the card in case the system allows more that one card per customer |  | Yes | String |  |  |
| Payment Card - Number | Credit card number |  | No | String |  |  |
| Payment Card - CVV | CVV on card |  | No | String |  |  |
| Payment Card - Type | Payment Card Type. Can be AMEX, BANKOFAMERICA, BARCLAYCARD, CAPITALONE, CARTASI, CARTEBLANCH, CARTEBLEUE, CHASE, CITI, DANKORT, DINERSCLUB, DISCOVERS, ELV, EPS, FIRSTPREMIER, GIROPAY, IDEAL, JCB, LASER, MAESTRO, MASTERCARD, MASTERCARDDEBIT, NICOS, PAYPAL, PLCC, SANTANDER, SOFORT, SWITCH, UKE, VISA |  | No | PaymentCardType |  |  |
| Payment Card - Card Holder Name | Card holder's name as it appears in the credit card |  | No | String |  |  |
| Payment Card - Valid From | Valid from date in the format YYYY/MM | 2001/02 | Yes | YearMonth |  |  |
| Payment Card - Expiration Date | Expiration date in the format YYYY/MM | 2015/03 | No | YearMonth |  |  |
| Payment Card - Issue Number | Issue Number | 01 | Yes | Integer |  |  |
| Payment Card - Billing Address | Customer's Billing Address |  | No | Address |  |  |
| Payment Card - Billing Address - Friendly Name | Address Friendly Name | Default Address | Yes | String |  |  |
| Payment Card - Billing Address - First Name | Address Person First Name | Joe | No | String |  |  |
| Payment Card - Billing Address - Last Name | Address Person Last Name | Bloggs | No | String |  |  |
| Payment Card - Billing Address - Line 1 | Address Line 1 | 1 Bryant Park | No | String |  |  |
| Payment Card - Billing Address - Line 2 | Address Line 2 | Suite 4444 | Yes | String |  |  |
| Payment Card - Billing Address - Line 3 | Address Line 3 | | Yes | String |  |  |
| Payment Card - Billing Address - Line 4 | Address Line 4 | | Yes | String |  |  |
| Payment Card - Billing Address - City | Address City | New York | No | String |  |  |
| Payment Card - Billing Address - County | Address County |  | Yes | String |  |  |
| Payment Card - Billing Address - State | Address State | NY | Yes | String |  |  |
| Payment Card - Billing Address - Post Code | Address Post Code | 10036 | No | String |  |  |
| Payment Card - Billing Address - Country | Address Country |  | No | String |  |  |
| Payment Card - Billing Address - Country - Name | Address Country Name | United States | No | String |  |  |
| Payment Card - Billing Address - Country - Alpha 2 Code | Address Country Alpha 2 Code | US | No | String |  |  |

Process Orders Response
-----------------------
| Name | Description | Example | Nullable | Type | | Notes |
| --- | --- | --- | --- | --- | --- | --- |
| Order Results | Set of Order Results |  | No | Array |  |  |
| Order ID | Order ID to associate processed order | 1 | No | String |  |  |
| Message | Message to determine status  | Purchase successful | No | String |  | Free text |
| Redirect URL | URL associated with order | http://link.to/order/1 | Yes | String |  |  |

Error Response
--------------
| Name | Description | Example | Nullable | Type | | Notes |
| --- | --- | --- | --- | --- | --- | --- |
| Code | Error Type | BAD_REQUEST | No | ErrorType | | |
| Message | Information about error | The request entity had validation errors | No | String | | |
| Validation Errors | Any validation errors | ["user.emailAddress may not be empty"] | Yes | Array | | |